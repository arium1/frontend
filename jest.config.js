module.exports = {
  modulePaths: ["<rootDir>"],
  testEnvironment: "jsdom",
  transform: {
    "^.+\\.(tsx?|ts?)$": "ts-jest",
    "^.+\\.(jsx?|js?)$": "babel-jest",
  
  },
  reporters: ["default", "jest-sonar"],
  globals: {
    __HTTP_HOST: "http://localhost:8090"
  },
  testRegex: "(/__tests__/.*|(\\.|/)(test))\\.(jsx?|tsx?|ts?)$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx"],
  moduleDirectories: ["webpack", "node_modules", "src"],
  testPathIgnorePatterns: ["/config/", "/public/", "/vendor/", "/.gems"],
  moduleNameMapper: {
    ".svg$": "jest-config.mock.js",
    ".scss$": "jest-config.mock.js",
    ".otf$": "jest-config.mock.js",
    ".jpeg$": "jest-config.mock.js",
    ".png$": "jest-config.mock.js",
    ".webp$": "jest-config.mock.js",
    ".jpg$": "jest-config.mock.js",
    "^uuid$": "node_modules/uuid/dist/esm-browser/index.js"
  },
  transformIgnorePatterns: [
    "/node_modules/(?!uuid).+\\.js$"
  ]
}
