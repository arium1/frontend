# DEV setup

Install dev dependencies:

###Mac

1. `apt-get update`
2. `apt-get install node`
3. `yarn` or `npm install`

Run in development mode

4. Run app for development
   - dev mode `yarn start` or `npm run start`

### Testing and updating

Run tests:

- `yarn jest` or `npm run test`

Run linter

- `yarn lint` or `npm run lint`

Run fixes by linter

- `yarn lint:fix` or `npm run lint:fix`

Run dll packages update

- `yarn dll` or `npm run dll`

# Prod setup

1. `apt-get update`
2. `apt-get install node`
3. `yarn` or `npm install`

Make production build

4.  `yarn build` or `npm run build`
5.  `cp -r build/ /path/to/static/files`

Change nginx settings

7. ```
   ...
   http2_push_preload on;

    location / {
        alias /path to project dir;
        try_files $uri /index.html =404;
    }

    location /v1/ {
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-NginX-Proxy true;

        proxy_pass http://hosttobackend:porttobackend/;
        proxy_redirect off;
    }

     ...
   ```

```

```
