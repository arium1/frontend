module.exports = (api) => {
  const cacheConfigExecuting = true
  api.cache(cacheConfigExecuting)

  return {
    plugins: [
      "@babel/plugin-proposal-class-properties",
      [
        "@babel/plugin-transform-runtime",
        {
          regenerator: true
        }
      ]
    ],
    presets: [
      [
        "@babel/env",
        {
          targets: "> 0.25%, not dead"
        }
      ],
      ["@babel/preset-react", { runtime: "automatic" }],
      [
        "@babel/preset-typescript",
        {
          isTSX: true,
          allExtensions: true
        }
      ]
    ]
  }
}
