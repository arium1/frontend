module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["eslint:recommended", "plugin:@typescript-eslint/recommended", "plugin:storybook/recommended"],
  parser: "@typescript-eslint/parser",
  plugins: ["@typescript-eslint", "prettier"],
  root: true,
  parserOptions: {
    project: "./tsconfig.json"
  },
  settings: {},
  rules: {
    "prettier/prettier": ["error", {
      endOfLine: "auto"
    }],
    "import/extensions": "off",
    "import/no-unresolved": "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    quotes: ["error", "double"],
    "react/jsx-one-expression-per-line": ["off", {
      allow: "single-child"
    }],
    "css-colonexpected": "off",
    "react/jsx-props-no-spreading": "off",
    "react/destructuring-assignment": "off",
    "react/require-default-props": "off",
    "sort-keys": "off",
    "@typescript-eslint/semi": "off",
    "@typescript-eslint/explicit-function-return-type": "off",
    "@typescript-eslint/quotes": "off",
    "@typescript-eslint/no-unused-vars": "error",
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/member-delimiter-style": "off",
    "@typescript-eslint/indent": "off",
    "@typescript-eslint/ban-ts-ignore": "off",
    "@typescript-eslint/no-explicit-any": "warn",
    "@typescript-eslint/no-var-requires": "off",
    "comma-dangle": "off",
    "object-curly-newline": "off",
    "implicit-arrow-linebreak": "off",
    "function-paren-newline": "off",
    "max-len": "off",
    "react/prop-types": "off",
    "linebreak-style": 0,
    "global-require": 0,
    "no-confusing-arrow": "off",
    "import/prefer-default-export": "off",
    "eslint linebreak-style": [0, "error", "windows"],
    "jsx-a11y/click-events-have-key-events": "off",
    "jsx-a11y/no-static-element-interactions": "off",
    "operator-linebreak": "off",
    "jsx-a11y/anchor-is-valid": "off",
    "no-irregular-whitespace": "off",
    "no-shadow": "off",
    "no-nested-ternary": "warn",
    "no-param-reassign": ["error", {
      props: false
    }]
  }
};