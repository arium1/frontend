/* eslint-disable indent */
const webpack = require("webpack")
const path = require("path")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")

const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin

const sourceDir = path.join(__dirname, "./src")
const entryPath = path.join(sourceDir, "./index.tsx")
const favicon = path.join(sourceDir, "./shared/assets/img/favicon.ico")

const packageJSON = require(path.join(__dirname, "./package.json"))

module.exports = require("./webpack.config.base")({
  mode: "production",
  devtool: "cheap-module-source-map",
  entry: {
    main: entryPath
  },
  target: "web",

  output: {
    filename: (pathData) => {
      return pathData.chunk.name === "main" ? "[name].[contenthash].js" : "[name].js"
    },
    chunkFilename: "[name].js",
    publicPath: "",
    asyncChunks: true
  },

  optimization: {
    usedExports: true,
    chunkIds: "size",
    moduleIds: "size",
    mangleExports: "size",
    emitOnErrors: true,
    minimize: true,
    sideEffects: false,
    nodeEnv: "production",
    flagIncludedChunks: true,
    concatenateModules: true,
    splitChunks: {
      chunks: "all",
      minSize: 15000,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 50,
      maxInitialRequests: 50,
      enforceSizeThreshold: 50000,
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,

          name(module) {
            // get the name. E.g. node_modules/packageName/not/this/part.js
            // or node_modules/packageName

            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1]

            // get the bundle version. At first get dependencies list
            const dependensiesList = packageJSON.dependencies
            // Get version to avoid cache problems
            const version = dependensiesList[packageName] ?? ""
            return `vendor.${packageName.replace("@", "")}${version ?? ""}`
          },

          priority: -10,
          reuseExistingChunk: true
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true
        }
      }
    }
  },

  plugins: [
    new webpack.DefinePlugin({
      PRODUCTION: JSON.stringify(true),
      "process.env": {
        NODE_ENV: JSON.stringify("production"),
        STAGE: JSON.stringify("build")
      }
    }),
    new webpack.DefinePlugin({
      "process.env.VERSION": JSON.stringify(process.env.npm_package_version)
    }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "static/[name].[hash:4].css",
      chunkFilename: "static/[id].[hash:4].css"
    }),

    new HtmlWebpackPlugin({
      template: "index.prod.ejs",
      production: true,
      favicon,
      inject: "head"
    }),

    new BundleAnalyzerPlugin({ analyzerMode: "static" })
  ],
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(ico|eot|otf|ttf|woff|woff2)(\?.*)?$/,
        include: sourceDir,
        use: {
          loader: "file-loader"
        }
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "../"
            }
          },
          {
            loader: "css-loader"
          },
          "sass-loader"
        ]
      },

      {
        test: /\.css$/,
        include: [/node_modules/],
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: "../"
            }
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              importLoaders: 1
            }
          },
          "postcss-loader"
        ]
      },

      {
        test: /\.(ts|tsx|js)$/,
        include: [sourceDir],
        use: [
          {
            loader: "babel-loader"
          }
        ]
      }
    ]
  }
})
