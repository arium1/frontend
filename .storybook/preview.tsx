import React from "react"
import type { Preview, StoryFn } from "@storybook/react"

import "../src/stories/index.scss"
import { Providers } from "../src/app/providers"

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/
      }
    }
  },
  decorators: [
    (Story: StoryFn) => {
      return (
        <Providers>
          <Story />
        </Providers>
      )
    }
  ]
}

export default preview
