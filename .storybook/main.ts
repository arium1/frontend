import type { StorybookConfig } from "@storybook/react-webpack5"
const path = require("path") // 👈 import path

const sourceDir = path.join(__dirname, "../src")

const config: StorybookConfig = {
  stories: ["../src/**/*.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
    {
      name: "@storybook/addon-styling",
      options: {
        sass: {
          implementation: require("sass")
        }
      }
    }
  ],

  framework: {
    name: "@storybook/react-webpack5",
    options: {
      builder: { fsCache: true, lazyCompilation: true }
    }
  },
  webpackFinal: async (config: any) => {
    // 👈 and add this here
    config.resolve.alias = {
      ...config.resolve.alias,
      src: sourceDir,
      lib: path.join(sourceDir, "shared/lib"),
      pages: path.join(sourceDir, "pages"),
      assets: path.join(sourceDir, "shared/assets"),
      shared: path.join(sourceDir, "shared"),
      features: path.join(sourceDir, "features"),
      "@": path.resolve(__dirname, "../src/")
    }
    return config
  },

  docs: {
    autodocs: "tag"
  }
}
export default config
