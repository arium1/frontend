import { Suspense } from "react"
import ReactDOM from "react-dom/client"
import { RouterProvider } from "react-router-dom"
import { Provider as ReduxProvider } from "react-redux"
import { ConfigProvider, theme } from "antd"

import dayjs from "dayjs"
import "dayjs/locale/ru"

import ErrorWrapper from "shared/ui/ErrorWrapper"

import { router } from "./routes"

import { appStore } from "./appStore"

import "./index.scss"

import s from "./app.module.scss"

dayjs.locale("ru")

export const renderApp = () => {
  const target = document.getElementById("root")
  target?.setAttribute("class", s.root)

  if (!target) {
    return
  }

  const root = ReactDOM.createRoot(target)
  const { defaultAlgorithm } = theme

  root.render(
    <ErrorWrapper>
      <ConfigProvider
        theme={{
          algorithm: defaultAlgorithm
        }}
      >
        <ReduxProvider store={appStore}>
          <Suspense fallback={null}>
            <RouterProvider router={router} />
          </Suspense>
        </ReduxProvider>
      </ConfigProvider>
    </ErrorWrapper>
  )
}
