import { Suspense } from "react"
import { BrowserRouter } from "react-router-dom"
import { Provider as ReduxProvider } from "react-redux"
import { ConfigProvider, theme } from "antd"

import ErrorWrapper from "shared/ui/ErrorWrapper"

import { appStore } from "./appStore"

const Providers = ({ children }: any) => {
  const { defaultAlgorithm } = theme
  return (
    <ErrorWrapper>
      <ConfigProvider
        theme={{
          algorithm: defaultAlgorithm
        }}
      >
        <ReduxProvider store={appStore}>
          <Suspense fallback={null}>
            <BrowserRouter>{children}</BrowserRouter>
          </Suspense>
        </ReduxProvider>
      </ConfigProvider>
    </ErrorWrapper>
  )
}

export { Providers }
