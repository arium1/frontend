import { configureStore, AnyAction } from "@reduxjs/toolkit"
import { setupListeners } from "@reduxjs/toolkit/query"

import { createEpicMiddleware } from "redux-observable"

import { rootReducer } from "./rootReducer"
import { rootEpic } from "./rootEpics"

export type MyState = ReturnType<typeof rootReducer>

export const epicMiddleware = createEpicMiddleware<AnyAction, AnyAction, MyState>()

export function makeStore() {
  const store = configureStore({
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware({
        thunk: false
      }).concat([epicMiddleware]),
    reducer: rootReducer
  })

  setupListeners(store.dispatch)

  return store
}

export const appStore = makeStore()

epicMiddleware.run(rootEpic as any)

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof appStore.getState>
export type AppDispatch = typeof appStore.dispatch
