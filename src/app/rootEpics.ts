import { combineEpics } from "redux-observable"

import { loginEpics } from "pages/LoginForm/data/login"

export const rootEpic = combineEpics(loginEpics)
