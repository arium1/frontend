import { combineReducers } from "@reduxjs/toolkit"

import login from "pages/LoginForm/data/login"

export const rootReducer = combineReducers({ login })
