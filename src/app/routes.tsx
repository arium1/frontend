import { createBrowserRouter } from "react-router-dom"

import LoginForm from "pages/LoginForm"

import { NotFoundPage } from "../shared/ui"

export const router = createBrowserRouter([
  {
    element: <LoginForm />,
    index: true,
    path: "/"
  },
  {
    element: <NotFoundPage />,
    path: "*"
  }
])
