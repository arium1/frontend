import { Observable as O } from "rxjs"
// import superagent from "superagent"
// import { pipe, toPairs, map, join, compose, isNil } from "ramda"

// const request = superagent.agent()

type Path = string
export type ApiPath = `${Path}`

type AnyParam = string | boolean | number | Array<any> | unknown

export type Params = { [k: string]: AnyParam } | Record<string, AnyParam>
type ExtraHeader = Record<string, string> | undefined
type Method = "post" | "get"

type SendRequest = <T, P extends Params>(
  verb: string,
  url: string,
  data: P | null,
  params?: P | null,
  extraHeaders?: ExtraHeader,
  type?: "blob"
) => O<T>

type GetRequest = <T, P extends Params = any>(url: string, params?: P, headers?: ExtraHeader) => O<T>
type DelRequest = <T, P extends Params = any>(url: string, data?: P, headers?: ExtraHeader) => O<T>
type Request = <T, P extends Record<string, unknown> = any>(url: string, data: P, headers?: ExtraHeader) => O<T>
type BlobRequest = <T, P extends Record<string, AnyParam> = any>(
  method: Method,
  url: string,
  params?: P,
  headers?: ExtraHeader
) => O<T>
// type BuildParams = <T>(params: T) => string
// type Headers = {
//   [key: string]: string
// }
// type FileType = "blob"

// export const HEADERS: Headers = {}

// const buildParams: BuildParams = pipe(
//   toPairs as any,
//   map(([key, value]) => `${key}=${encodeURIComponent(value)}`),
//   join("&")
// )

// const buildUrl = (verb: string, params: string) =>
//   compose((url: string) => {
//     if (verb === "get" && params.length > 0) {
//       return `${url}?${params}`
//     }
//     if (verb === "get" && params.length === 0) {
//       return `${url}`
//     }

//     return params.length > 0 ? `${url}?${params}` : `${url}`
//   })

const sendRequest: SendRequest = <T>() =>
  // verb: string,
  // url: string,
  // data: P | null,
  // params: null | P,
  // extraHeaders: ExtraHeader,
  // type: FileType | undefined
  new O<T>((observer) => {
    // const paramsStr = isNil(params) ? "" : buildParams<P>(params)
    // const fullUrl = buildUrl(verb, paramsStr)(url)
    // const reques: any = request

    // const req = reques[verb](fullUrl).accept("application/json")

    // Object.keys(HEADERS).forEach((key: any) => {
    //   req.set(key, HEADERS[key])
    // })

    // if (extraHeaders) {
    //   Object.keys(extraHeaders).forEach((extraKey: any) => {
    //     req.set(extraKey, extraHeaders[extraKey])
    //   })
    // }

    // if (type) {
    //   req.responseType(type)
    // }

    // if (data) {
    //   req.send(data)
    // }

    const req = new Promise<{ status: 200; err: boolean; body: { token: string } }>((res) => {
      return setTimeout(() => {
        res({ status: 200, err: false, body: { token: "hbgu9n2u39?A|?SR#3b9usd22" } })
      }, 3000)
    })

    req.then((res) => {
      if (res.status > 499) {
        observer.error(res.status)
      }
      if (res.status > 399 && res.status < 499) {
        observer.error(res.status)
      }
      if (res.err) {
        observer.error(res.status)
      } else {
        observer.next(res && (res.body as any))
      }
      observer.complete()
    })
    return () => req.finally()
  })

export const get: GetRequest = <T, P extends Params>(url: string, params?: P, headers?: ExtraHeader) =>
  sendRequest<T, P>("get", url, null, params, headers)

export const getBlobFile: BlobRequest = <T, P extends Record<string, unknown>>(
  method: Method,
  url: string,
  params?: P,
  headers?: ExtraHeader
) => sendRequest<T, P>(method, url, params ?? null, null, headers, "blob")

export const post: Request = <T, P extends Record<string, unknown>>(url: string, data: P, headers?: ExtraHeader) =>
  sendRequest<T, P>("post", url, data, null, headers)

export const put: Request = <T, P extends Record<string, unknown>>(url: string, data: P, headers?: ExtraHeader) =>
  sendRequest<T, P>("put", url, data, null, headers)

export const del: DelRequest = <T, P extends Params>(url: string, data?: P, headers?: ExtraHeader) =>
  sendRequest<T, P>("delete", url, data ?? null, null, headers)
