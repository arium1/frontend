import { Result } from "antd"

import s from "./not_found.module.scss"

const NotFoundPage = () => (
  <div className={s.not_found_page}>
    <Result status="404" title="404" subTitle="Sorry, the page you visited does not exist." />
  </div>
)

export default NotFoundPage
