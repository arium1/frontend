import { FC } from "react"

import s from "./TextInput.module.scss"

type Props = {
  error?: string
  type: string
  label: string
  placeholder?: string
}

const TextInput: FC<Props> = ({ error, type, placeholder, label, ...args }) => {
  return (
    <div className={s.input_container}>
      <div>
        <label>{label}</label>
      </div>
      <input type={type} {...args} className={s.text_input} placeholder={placeholder} />
      {error && <p className="text-xs">{error}</p>}
    </div>
  )
}

export { TextInput }
