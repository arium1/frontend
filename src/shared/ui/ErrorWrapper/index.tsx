import { PureComponent } from "react"

class ErrorBoundary extends PureComponent<any, any> {
  constructor(props: any) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  componentDidCatch() {
    this.setState({
      hasError: true
    })
  }

  render() {
    const { hasError } = this.state

    if (hasError) {
      return <div className="error-wrapper">Something went wrong</div>
    }

    return this.props.children
  }
}

export default ErrorBoundary
