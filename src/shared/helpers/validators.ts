export const maskNumber = (str: string) => {
  const pattern = "_ (___) ___-__-__" as const
  let i = 0
  const val = str.replace(/\D/g, "")

  const result = pattern.replace(/./g, function (a) {
    return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
  })

  return result
}
