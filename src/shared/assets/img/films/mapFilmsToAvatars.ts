import NewHope from "./newhope.jpeg"
import EmpireStrikes from "./empirestrikes.jpeg"
import ReturnJedi from "./returnjedi.jpeg"
import PhantomMenace from "./phantommenace.jpeg"
import AttackClones from "./attackclones.jpeg"
import RevengeSith from "./revengesith.jpeg"

export const mapFilmsToAvatars = (name: string) => {
  if (name.toLowerCase().includes("hope")) {
    return NewHope
  }
  if (name.toLowerCase().includes("empire strikes")) {
    return EmpireStrikes
  }
  if (name.toLowerCase().includes("return of the jedi")) {
    return ReturnJedi
  }
  if (name.toLowerCase().includes("phantom menace")) {
    return PhantomMenace
  }
  if (name.toLowerCase().includes("attack of the clones")) {
    return AttackClones
  }
  if (name.toLowerCase().includes("revenge of the sith")) {
    return RevengeSith
  }

  return ""
}
