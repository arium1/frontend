import R2 from "./R2-D2.webp"
import Leia from "./leia.jpeg"
import Luke from "./luke.jpeg"
import DarthVader from "./darth-vader.jpg"
import C3PO from "./c-3po.jpeg"

export const mapToAvatars = (name: string) => {
  if (name.toLowerCase().includes("3po")) {
    return C3PO
  }
  if (name.toLowerCase().includes("darth")) {
    return DarthVader
  }
  if (name.toLowerCase().includes("luke")) {
    return Luke
  }
  if (name.toLowerCase().includes("leia")) {
    return Leia
  }
  if (name.toLowerCase().includes("r2")) {
    return R2
  }
  return ""
}
