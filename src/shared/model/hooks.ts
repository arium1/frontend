import { useSelector, type TypedUseSelectorHook, useDispatch } from "react-redux"

import { RootState, AppDispatch } from "app/appStore"

export const useAppDispatch = useDispatch<AppDispatch>
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
