export type LoginData = { login: string; password: string }
export type LoginTokenResponse = { token: string }

export enum Sex {
  MALE = "MALE",
  FEMALE = "FEMALE"
}

export type LoginForm = {
  surname: string
  name: string
  familyname: string | null
  sex: Sex | null
  birthday: string
  phone: string
  mail: string
  adress: string | null
  employer: string | null
}
