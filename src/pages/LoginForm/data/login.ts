import { createSlice, createAction } from "@reduxjs/toolkit"
import type { PayloadAction, Action } from "@reduxjs/toolkit"

import { combineEpics, Epic } from "redux-observable"
import { switchMap, map, catchError, of, filter } from "rxjs"

import { post } from "shared/api"

import { LoginForm, LoginTokenResponse } from "./types"

interface LoginState {
  isLogin: boolean
  token: string
}

const initialState: LoginState = { isLogin: false, token: "" }

export const loginStart = createAction<LoginForm>("loginStart")

const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    loginDone(state, action: PayloadAction<LoginTokenResponse>) {
      state.isLogin = false
      state.token = action.payload.token
    },
    loginFailed(state) {
      state.isLogin = false
    }
  },
  extraReducers: (builder) => {
    builder.addCase(loginStart, (state) => {
      state.isLogin = true
    })
  }
})

export const { loginDone, loginFailed } = loginSlice.actions
export default loginSlice.reducer

const postLoginDataEpic$: Epic<Action<(typeof loginStart)["match"]>, Action<any>, void, any> = (action$) =>
  action$.pipe(
    filter(loginStart.match),
    switchMap(({ payload }) =>
      post<LoginTokenResponse, LoginForm>("/api/people", payload).pipe(
        map((response) => loginDone(response)),
        catchError((err) => of(loginFailed(err)))
      )
    )
  )

export const loginEpics = combineEpics(postLoginDataEpic$)
