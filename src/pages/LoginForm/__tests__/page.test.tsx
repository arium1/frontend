import { render, screen } from "@testing-library/react"
import { rest } from "msw"
import { setupServer } from "msw/node"

import { Providers } from "app/providers"

import { appStore } from "app/appStore"
import { loginStart } from "../data/login"

import HomePage from "../"
import { Sex } from "../data/types"

const mockData = {
  body: { token: "342498432984" }
}

const server = setupServer(
  rest.all("/api/people", (_, res, ctx) => {
    return res(ctx.json(mockData))
  })
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

describe("Test home page", () => {
  it("render home page", () => {
    render(
      <Providers>
        <HomePage />
      </Providers>
    )
    expect(screen.getByTestId("home-page")).toBeDefined()
  })
  it("render home page to snapshot", () => {
    const { asFragment } = render(
      <Providers>
        <HomePage />
      </Providers>
    )
    expect(asFragment()).toMatchSnapshot()
  })
  it("Test home page redux state", () => {
    appStore.dispatch(
      loginStart({
        surname: "Smith",
        name: "John",
        familyname: "Michael",
        sex: Sex.MALE,
        birthday: "22.11.1992",
        phone: "8-929-684-22-46",
        mail: "johndoe@gmail.com",
        adress: null,
        employer: null
      })
    )
    expect(appStore.getState().login.isLogin).toBeTruthy()
  })
})
