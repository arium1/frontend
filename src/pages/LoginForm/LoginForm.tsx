import { Col, Row, Form, Input, DatePicker, Typography, Select, Button } from "antd"
import dayjs from "dayjs"

import { emailRegex } from "shared/helpers/regexp"
import { maskNumber } from "shared/helpers/validators"

import { useAppDispatch, useAppSelector } from "shared/model"

import { Sex, type LoginForm as LoginFormType } from "./data/types"
import { loginStart } from "./data/login"

import s from "./home.module.scss"

const sexOptions = [
  { label: Sex.FEMALE, value: Sex.FEMALE },
  { label: Sex.MALE, value: Sex.MALE }
]

export const LoginForm = () => {
  const [formInstance] = Form.useForm<LoginFormType>()
  const dispatch = useAppDispatch()

  const isLogin = useAppSelector((state) => state.login.isLogin)

  const onSubmit = async () => {
    const data = formInstance.getFieldsValue()
    try {
      await formInstance.validateFields()
      dispatch(loginStart({ ...data, birthday: dayjs(data.birthday).format("DD.MM.YYYY") }))
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <Col className={s.loginForm}>
      <Typography.Title>Информация о сотруднике</Typography.Title>
      <Form
        form={formInstance}
        layout="vertical"
        className={s.loginFormInstance}
        onSubmitCapture={onSubmit}
        disabled={isLogin}
      >
        <Form.Item
          label=""
          colon={false}
          name="surname"
          rules={[
            {
              required: true,
              message: "Поле является обязательным"
            }
          ]}
        >
          <Input size="large" placeholder="Фамилия" />
        </Form.Item>
        <Form.Item
          label=""
          colon={false}
          name="name"
          rules={[
            {
              required: true,
              message: "Поле является обязательным"
            }
          ]}
        >
          <Input size="large" placeholder="Имя" />
        </Form.Item>
        <Form.Item
          label=""
          colon={false}
          name="familyname"
          rules={[
            {
              required: false,
              message: "Поле является обязательным"
            }
          ]}
        >
          <Input size="large" placeholder="Отчество" />
        </Form.Item>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label=""
              colon={false}
              name="sex"
              rules={[
                {
                  required: false,
                  message: "Поле является обязательным"
                }
              ]}
            >
              <Select size="large" placeholder="Пол" options={sexOptions} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label=""
              colon={false}
              name="birthday"
              rules={[
                {
                  required: true,
                  message: "Поле является обязательным"
                }
              ]}
            >
              <DatePicker size="large" placeholder="Дата рождения" format="DD.MM.YYYY" style={{ width: "100%" }} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={12}>
            <Form.Item
              label=""
              colon={false}
              name="examDate"
              normalize={maskNumber}
              rules={[
                {
                  required: true,
                  message: "Обязательное поле"
                }
              ]}
            >
              <Input size="large" placeholder="Мобильный телефон" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label=""
              colon={false}
              name="mail"
              rules={[
                {
                  required: true,
                  message: "Введен некорректный адрес почты",
                  validator: (_, value) => {
                    if (emailRegex.test(value)) {
                      return Promise.resolve()
                    }
                    return Promise.reject(new Error("Введен некорректный адрес почты"))
                  }
                }
              ]}
            >
              <Input size="large" placeholder="Email (необязательно)" />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          label=""
          colon={false}
          name="adress"
          rules={[
            {
              required: false
            }
          ]}
        >
          <Input size="large" placeholder="Адрес постоянной регистрации" />
        </Form.Item>
        <Form.Item
          label=""
          colon={false}
          name="employer"
          rules={[
            {
              required: false
            }
          ]}
        >
          <Input size="large" placeholder="Название работодателя" />
        </Form.Item>
        <Form.Item>
          <Row justify="end">
            <Button type="primary" htmlType="submit" size="large" loading={isLogin}>
              Сохранить
            </Button>
          </Row>
        </Form.Item>
      </Form>
    </Col>
  )
}
