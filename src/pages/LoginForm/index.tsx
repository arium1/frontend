import { LoginForm } from "./LoginForm"

import s from "./home.module.scss"

const LoginFormPage = () => {
  return (
    <div className={s.homePage} data-testid="home-page">
      <LoginForm />
    </div>
  )
}

export default LoginFormPage
