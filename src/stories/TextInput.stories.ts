import type { Meta, StoryObj } from "@storybook/react"

import { TextInput } from "../shared/ui/TextInput/TextInput"

// More on how to set up stories at: https://storybook.js.org/docs/react/writing-stories/introduction
const meta = {
  title: "Example/TextInput",
  component: TextInput
} satisfies Meta<typeof TextInput>

export default meta
type Story = StoryObj<typeof meta>

// More on writing stories with args: https://storybook.js.org/docs/react/writing-stories/args
export const SimpleButton: Story = {
  args: {
    placeholder: "Search any value...",
    label: "Password",
    type: "text"
  }
}

export const SimpleButtonWithError: Story = {
  args: {
    placeholder: "Search any value...",
    label: "Password",
    type: "text",
    error: "Something went wrong"
  }
}
