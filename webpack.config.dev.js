/* eslint-disable indent */
const path = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

const sourceDir = path.join(__dirname, "./src")
const dllManifest = require("./dll/libs-manifest.json")

const entryPath = path.join(sourceDir, "./index.tsx")
const favicon = path.join(sourceDir, "./shared/assets/img/favicon.ico")

const host = process.env.HOST || "localhost"
const port = process.env.PORT || 8000

module.exports = require("./webpack.config.base")({
  mode: "development",

  entry: [
    entryPath,
    "webpack/hot/dev-server.js",
    // Dev server client for web socket transport, hot and live reload logic
    "webpack-dev-server/client/index.js?hot=true&live-reload=true"
    // the entry point of our app
  ],

  devtool: "eval-source-map",
  output: {
    filename: "[name].js",
    chunkFilename: "[name].js",
    publicPath: "/"
  },

  optimization: {
    emitOnErrors: true,
    splitChunks: {
      chunks: "all",
      minSize: 1000,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 50,
      maxInitialRequests: 50,
      enforceSizeThreshold: 50000
    }
  },

  plugins: [
    // import dll manifest
    new webpack.DllReferencePlugin({
      context: path.resolve(__dirname, "."),
      manifest: dllManifest
    }),
    new HtmlWebpackPlugin({
      chunks: ["main"],
      filename: "index.html",
      template: "index.dev.ejs",
      favicon,
      inject: "head"
    }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "static/[name].[hash:4].css",
      chunkFilename: "static/[id].[hash:4].css"
    }),

    // make DLL assets available for the app to download
    new AddAssetHtmlPlugin([{ filepath: require.resolve("./dll/libs.dll.js") }]),
    // Plugin for hot module replacement
    new webpack.HotModuleReplacementPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(ico|eot|otf|ttf|woff|woff2)(\?.*)?$/,
        include: sourceDir,
        use: {
          loader: "file-loader"
        }
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              modules: true
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.css$/,
        include: [/node_modules/],
        use: [
          {
            loader: "style-loader",
            options: {
              publicPath: "../"
            }
          },
          {
            loader: "css-loader",
            options: {
              modules: true,
              importLoaders: 1
            }
          },
          "postcss-loader"
        ]
      },
      {
        test: /\.(ts|tsx|js)$/,
        include: sourceDir,
        use: [
          {
            loader: "babel-loader",
            options: {
              cacheDirectory: path.join(__dirname, ".babel-cache", "dev")
            }
          }
        ]
      }
    ]
  }
})
