/* eslint-disable indent */
const path = require("path")

const sourceDir = path.join(__dirname, "./src")
const buildDir = path.join(__dirname, "./build")

// replace localhost with 0.0.0.0 if you want to access
// your app from wifi or a virtual machine
const host = process.env.HOST || "localhost"
const port = process.env.PORT || 8000
const protocol = process.env.PROTOCOL || "http"

const servePath = `${protocol}://${host}:${port}/`

const stats = {
  assets: true,
  children: false,
  chunks: false,
  hash: false,
  modules: false,
  publicPath: false,
  timings: true,
  version: false,
  warnings: true,
  colors: {
    green: "\u001b[32m"
  }
}

module.exports = (config) => ({
  mode: config.mode,
  optimization: config.optimization,
  entry: config.entry,
  module: config.module,
  devtool: config.devtool,

  context: sourceDir,

  output: Object.assign(
    {
      path: buildDir,
      publicPath: servePath
    },
    config.output
  ),

  plugins: []
    // join with prod/dev plugins
    .concat(config.plugins)
    // removed null plugins
    .filter(Boolean),

  resolve: {
    extensions: [".js", ".json", ".tsx", ".ts", ".less"],
    modules: ["node_modules"],
    alias: {
      src: sourceDir,
      lib: path.join(sourceDir, "shared/lib"),
      pages: path.join(sourceDir, "pages"),
      assets: path.join(sourceDir, "shared/assets"),
      shared: path.join(sourceDir, "shared"),
      features: path.join(sourceDir, "features"),
      components: path.join(sourceDir, "components")

    },
    symlinks: false
  },

  stats,

  devServer: {
    compress: false,
    client: false,
    historyApiFallback: {
      rewrites: [{ from: /./, to: "/index.html" }]
    },
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    hot: true,
    host,
    liveReload: false,
    open: true,
    port,
    proxy: {
      "/api/**": {
        target: "https://swapi.dev",
        secure: false,
        strictSSL: false
      }
    }
  }
})
